# Description
PIC compiler made for educational purposes in course IAG0450 - Analysis of Programming Languages (2016).

[A few supplementary slides](https://slides.com/hanneskinks/picantlr)

# Microprocessor
For testing **PIC16F690** is used with PICkit2.
[Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/41262E.pdf)

## Initialization
Without having any lines of input code, the compiler still produces 
a few lines of assembler code for initialization: 
``` asm
    ; set config bits
	__CONFIG 0x30D4
	; set the beginning address
	ORG 0x00
	
	; switch to bank 0
	BCF STATUS,RP0
	BCF STATUS,RP1
	
	; clear values on ports
	CLRF PORTA
	CLRF PORTB
	CLRF PORTC
	
	; switch to bank 1
	BSF STATUS,RP0
	BCF STATUS,RP1
	
	; set the PORT directions
	MOVLW 0xf0
	MOVWF TRISC
	MOVLW 0xff
	MOVWF TRISB
	MOVLW 0xff
	MOVWF TRISA
	
	; switch to bank 0
	BCF STATUS,RP0
	BCF STATUS,RP1
```

TRISA registers are used to configure ports either to input or output.
For example to use LEDs that are connected to PORTC, then TRISC should 
be set to 0 on the corresponding ports.

In case of the board used in lab, the connections can be seen here:

![IOtable](http://hkinks.com/upload/85538b.png)

The LED bits should be zeroes and the rest ones in order to be used as inputs (also
unused IO ports should be set as inputs). As a result 0xf0 should be written
 to PORTC.

[More details about microcontroller IO ports](http://learn.mikroe.com/ebooks/picmicrocontrollersprogramminginassembly/chapter/io-ports/)

Also in the very end the following lines are added, which essentally
 stop the execution of program:
``` asm
goto $
END
```
Dollar sign means address of current instruction - therefore it gets stuck in a loop.


	

# Example ASM
## LED Counter
``` asm
#include <p16f690.inc>

	__CONFIG 0x30D4
	ORG 0x00
	BCF STATUS,RP0
	BCF STATUS,RP1
	CLRF PORTA
	CLRF PORTB
	CLRF PORTC
	BSF STATUS,RP0
	BCF STATUS,RP1
	MOVLW 0xf0
	MOVWF TRISC
	MOVLW 0xff
	MOVWF TRISB
	MOVLW 0xff
	MOVWF TRISA
	BCF STATUS,RP0
	BCF STATUS,RP1
main
	MOVLW 0x00
	MOVWF PORTC
	MOVLW 0x00
	MOVWF 0x20
begin0
	MOVF 0x20, 0
	DECFSZ 16, 0
	GOTO branch0
	GOTO out0
branch0
	MOVF 0x20, 0
	MOVWF PORTC
	MOVLW 0x3
	MOVWF 0x72
	MOVLW 0x26
	MOVWF 0x71
	MOVLW 0x66
	MOVWF 0x70
delay0
	DECFSZ 0x70, 1
	GOTO delay0
	DECFSZ 0x71, 1
	GOTO delay0
	DECFSZ 0x72, 1
	GOTO delay0
	MOVF 0x20, 0
	ADDLW 1
	MOVWF 0x20
	GOTO begin0
out0
    goto $
    END
```

# Tutorial
Start by cloning the repository and creating a IDEA project out of it.
The repository includes a basic ANTLR skeleton for a compiler, using Visitor Design pattern.
The skeleton project has a few basic arguments you can use by editing ``Run->Edit Configurations->Program arguments``:
```
usage: picoLang
 -h         Print usage information.
 -i <arg>   Source file given as an input. If not specified, code will be
            entered from standard input.
 -o <arg>   Specify output file containing code compiled into assembler.
 -t         Show parse tree instead of running the code. Also no output
            will be generated.
 -v         Verbose.
```

For example, if you wish to draw parse tree for debugging purposes, then you should add ``-t`` to the ``Program arguments`` field.

## Turning LED on
To make the compiler able to interpret value assignments to LED we need to define assignment operation, LED and value types.
Essentially we want the compiler to be able to produce ASM for something like this:
```
LED = 1;
```

The basic grammar to recognize it, could look something like this ( with a few additions included in the skeleton project like delay, support for parenthesis, blocks and comments):

```
grammar Comp;
// parser rules
prog:  (line)+;

line: stmt SEMICOLON; // semicolon is mandatory

stmt: DELAY             # delayStmt
    | LED '=' expr      # ledStmt
    ;

expr: '(' expr ')'      # parens
    | INT               # int
    | HEX               # hex
	;

block:  '{' (stmt)* '}' ;


// lexical(token) rules
INT: [0-9]+;
HEX: '0x'[0-9]+;
LED: ('led' | 'LED');
DELAY: ('delay' | 'DELAY') '()'?;
SEMICOLON: ';';
COMMENT: '//' .*? ('\n'|EOF) -> skip;
MULTILINECOMMENT: '/*' .*? '*/' -> skip;
EOL: [ \t\r\n] -> skip;
```

Next, we should generate the lexer and parser java code using ANTLR. (In IDEA: Right click on *.g4 file and select "Generate ANTLR Recognizer")
Then we can modify ``EvalVisitor``, which is a subclass of generated ``BaseVisitor`` class. It is used to visit the Parse Tree and execute application specific code when visiting elements of the parsed input. This is where we translate our higher level language input to assembler instructions. 
First rule of interest is when the ``ledStmt`` is visited. To execute some code at that point, we should overwrite the methdod ``visitLedStmt``.
``` java
    @Override
    public Object visitLedStmt(CompParser.LedStmtContext ctx) {
        visit(ctx.expr()); // lets visit the INT to get the value
        lineArray.add("\tMOVWF PORTC");
        return null;
    }
```

Upon execution, it will first of all visit another rule, the expression (``expr``) associated with the assignment operation, which will get the actual numerical value what will be assigned to our LED. Then it will add ASM instruction ``MOVWF PORTC`` to a list of instructions (``lineArray``). This list of instructions is simply written into *.asm file in the end to be translated into bytecode and loaded onto the PIC. ``\tMOVWF PORTC`` instruction will simply move the value currently in the Working register (accumulator) to PORTC (0x07) where the LEDs are connected (
[IOtable](http://hkinks.com/upload/85538b.png) 
). The ``\t`` simply adds a tabulator in front for proper formating of the output code.
Next, we need to implement the visiting operation of that expression that actually gets the value of assignment. As the expr can descend into multiple rules, let's focus on the integer value at this point, by overwriting the ``visitInt`` method.
``` java
    @Override
    public Object visitInt(CompParser.IntContext ctx) {
        String value = ctx.getText();
		String hexValue = asHex(value);
        lineArray.add("\tMOVLW " + hexValue);
        return value;
    }
```
First of all, we get the actual value of the assignment as a string with ``ctx.getText()``. Then we should convert the integer into hexadecimal number. For that purpose there is a helping function ``asHex`` which you can use. Another line is added to the list of instructions, ``MOVLW 0x01``, which will load the literal value to the working register. Although not directly necessary at this point, we can also return the value of the integer also back to the method that called it out.

At this point our LED assignment input progam should output assembler looking something like this:
``` asm
	__CONFIG 0x30D4
	ORG 0x00
	BCF STATUS,RP0
	BCF STATUS,RP1
	CLRF PORTC
	BSF STATUS,RP0
	BCF STATUS,RP1
	MOVLW 0xf0
	MOVWF TRISC
	BCF STATUS,RP0
	BCF STATUS,RP1
main
	MOVLW 0x0f
	MOVWF PORTC
    goto $
    END
```
When running it in MPLABX make sure 
``` c
#include <p16f690.inc>
```
is included in the beginning in order for the address variables to work.

As a result the development board should simply turn on first LED.

## Adding delay
The following assembly code will produce a 1 sec delay. This has been also already implemented in the skeleton code.
``` asm
	MOVLW 0xB
	MOVWF 0x72
	MOVLW 0x26
	MOVWF 0x71
	MOVLW 0x66
	MOVWF 0x70
delay0
	DECFSZ 0x70, 1
	GOTO delay0
	DECFSZ 0x71, 1
	GOTO delay0
	DECFSZ 0x72, 1
	GOTO delay0
```

If you wish to understand it better or modify it for a smaller delay, check this out: [Delays explained](http://www.dos4ever.com/PICdelay/PICdelay.html)

To test the dealy, you can make a straight forward program, which will simply turn LED on and off few times without any loops:
```
LED = 0;
DELAY();
LED = 1;
DELAY();
LED = 0;
DELAY();
LED = 1;
DELAY();
LED = 0;
DELAY();
LED = 1;
DELAY();
```

## Creating loops
In order to create some simple while loops we need to start again by defining new parser rules. For the while statement itself we need to add a new statement (``stmt``) rule, something like that:
``` antlr
WHILE '('expr')' block    # whileStmt
```

Again, at this point I am going to simplify things a little and start from creating an endless loop with ``while(true)`` statement. So for that we will also need a boolean type under expression (``expr``) statemnt: 

``` antlr
BOOL              # boolean
```

And finally lexer rule for the ``BOOL``:

``` antlr
BOOL: ('true' | 'TRUE' | 'false' | 'FALSE' );
```

Generate ANTLR recognizer once again as the parser and lexer need refreshing every time you change the rules. Next we go back to ``EvalVisitor`` and overwrite the ``visitWhileStmt`` method.
``` java
    @Override
    public Object visitWhileStmt(CompParser.WhileStmtContext ctx) {
        labelNum = labelCounter;
        lineArray.add("begin" + labelNum );
        visit(ctx.cond);
        lineArray.add("branch"+labelNum);
        visit(ctx.block());
        lineArray.add("\tGOTO begin"+labelNum);
        lineArray.add("out"+labelNum);
        labelNum--;
        return null;
    }
```

For creating loops, we need to be able to implement jumps effectively. Assembler labels can be used and to manage them we need two global integer type variables: ``labelCounter`` and ``labelNum``. ``labelCounter`` as the name states, simply counts how many labels have been already used in the program and therefore helps do identify them. ``labelNum`` is for keeping track of the while loop we are currently in, so we know which label should we jump to when needed. 

We start off by stating that the current ``labelNum`` is whereever we are with our counting of labels. Then we create the beginning label, by naming it 'begin' and giving it the identifying number from ``labelNum``. This allows us to track where to jump, when we want to get back to the beginning of while loop.

Next we visit the condition statement, which checks if the conditions are set for continuing with the loop or not. Later on we will define code which will be inserted inbetween to this point.

Now however, we can simply continue by stating where the beginning of the while execution block is, with the label ``branch``. Again identified by the labelNum.
After the label, we will visit the ``block``, which includes all the code described in the while block, between the brackets. This again will simply insert all the assembler instruction inbetween there.

In the end of the block we should be returning back to the beginning, to check if the conditions are still set for another loop. This is done with ``GOTO`` instruction, that gives the begin label with correct identificator number as an operand to it.
The last label we need to add is where we exit the while statement and run the rest of the code. This is named as ``out``.
Finally we decrease the ``labelNum``. This is necessary for nested while statements. As the ``lableNum`` is global variable, then if the block of while includes another while, we overwrite the ``labelNum`` with an incremented ``labelCounter`` and when returning to the parent while statement, the the ``GOTO begin#`` and ``out#`` labels will have a wrong identificator. Therefore, we need to decrease it before exiting.


Now we only need to describe what visiting a boolean does:
``` java
    @Override
    public Object visitBool(CompParser.BoolContext ctx) {
        if (ctx.getText().toLowerCase().equals("true")) {
            lineArray.add("\tGOTO branch"+ labelNum);
        } else {
            lineArray.add("\tGOTO out"+ labelNum);
		}
        return ctx.getText().toLowerCase();
    }
```
Here we simply check whether the boolean value is ``true`` or ``false``. If it is ``true``, we add a ``GOTO`` line which will jump right into the ``branch`` label, where the execution of while block should reside in.
In case of ``false``, we jump out of the while loop, by goint to label ``out``.


For the block rule, there is no need to overwrite any particular methods, as the rule descends into ``stmt`` and the statements' visitors have been all described by now.

For testing you can give it an input something like this:
``` 
WHILE(TRUE) {
    LED = 0;
    DELAY();
    LED = 1;
    DELAY();
}
```

And as a result you should end up with assembler code, which would make the LED blink forever.
