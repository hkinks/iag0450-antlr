grammar Comp;

// parser rules
prog:  (line)+;

line: stmt ; // semicolon is mandatory

stmt: LED '=' expr SEMICOLON    # ledStmt
    | WHILE '(' cond ')' block  # whileExpr
    | DELAY SEMICOLON           # delayStmt
    ;
cond: expr              # condStmt
    ;
expr: '(' expr ')'      # parens
    | INT               # int
    | HEX               # hex
	;

block:  '{' (stmt)* '}' ;


// lexical(token) rules
HEX: '0x' [0-9A-Fa-f]+;
INT: [0-9]+;
LED: ('led' | 'LED');
WHILE: ('while' | 'WHILE');
DELAY: ('delay' | 'DELAY') '()'?;
SEMICOLON: ';';
COMMENT: '//' .*? ('\n'|EOF) -> skip;
MULTILINECOMMENT: '/*' .*? '*/' -> skip;
EOL: [ \t\r\n] -> skip;

