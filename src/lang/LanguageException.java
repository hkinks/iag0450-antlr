package lang;

/**
 * User: Hannes Kinks
 * Date: 12/14/13
 * Time: 5:29 PM
 */
public class LanguageException extends RuntimeException {
    public LanguageException(String message) {
        // show error and exit
        System.err.println(message);
        System.exit(1);
    }

    public LanguageException(String s, int line) {
        System.err.println("[line ".concat(String.valueOf(line)).concat("]  ").concat(s));
        System.exit(1);
    }
}
