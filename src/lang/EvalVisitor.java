package lang;
import java.util.*;

/**
 * Visitors for going through the parse tree.
 * @author Hannes Kinks
 * Date: 12/13/13
 * Date: 20/11/16
 */
public class EvalVisitor extends CompBaseVisitor<Object> {
    private static final int MEM_SIZE = 127;  // maximum allowed memory address
    private static final String ADDRESS_FORMAT = "0x%02x";

    // var memory counter, initial address where we start storing variables.
    private int adrCounter = 0x20;
    // map for remembering which variable has been assigned to which memory address
    private Map<String, Integer> variables = new LinkedHashMap<String, Integer>(MEM_SIZE);

    ArrayList<String> lineArray = new ArrayList<String>(); // list for keeping the assembler code lines
    private boolean verbose; // are we showing debug messages?
    private int delayCounter = 0; // delay label's counter

    private int labelCounter = 0; // for keeping track of the labels
    private int currentLabel = 0;

    /**
     * Constructor for setting the verbosity and writing first initialization lines of ASM
     * @param verbose Should the debug messages be shown?
     */
    EvalVisitor(boolean verbose) {
        // do initialization
        lineArray.add("\t__CONFIG 0x30D4"); // sets config bits
        lineArray.add("\tORG 0x00"); // sets from which address memory will be written into
        initializePorts();
        setPortDirections();

        lineArray.add("main");
        this.verbose = verbose;
    }

    private void setPortDirections() {
        switchBank(1);
        lineArray.add("\tMOVLW 0xf0");  // first 4 input, last 4 output for leds
        lineArray.add("\tMOVWF TRISC");
        lineArray.add("\tMOVLW 0xff");
        lineArray.add("\tMOVWF TRISB");
        lineArray.add("\tMOVLW 0xff");
        lineArray.add("\tMOVWF TRISA");
        switchBank(0);
    }

    private void initializePorts() {
        switchBank(0);
        lineArray.add("\tCLRF PORTA");
        lineArray.add("\tCLRF PORTB");
        lineArray.add("\tCLRF PORTC");
    }

    private void switchBank(int bankNum) {
        switch (bankNum) {
            case 0:
                lineArray.add("\tBCF STATUS,RP0");
                lineArray.add("\tBCF STATUS,RP1");
                break;
            case 1:
                lineArray.add("\tBSF STATUS,RP0");
                lineArray.add("\tBCF STATUS,RP1");
                break;
            case 2:
                lineArray.add("\tBCF STATUS,RP0");
                lineArray.add("\tBSF STATUS,RP1");
                break;
            case 3:
                lineArray.add("\tBSF STATUS,RP0");
                lineArray.add("\tBSF STATUS,RP1");
                break;
        }
    }

    @Override
    public Object visitLedStmt(CompParser.LedStmtContext ctx) {
        visit(ctx.expr());
        lineArray.add("\tMOVWF PORTC");
        return null;
    }

    @Override
    public Object visitHex(CompParser.HexContext ctx) {
        String stringValue = ctx.getText();
        lineArray.add("\tMOVLW " + stringValue);
        return stringValue;
    }

    @Override
    public Object visitInt(CompParser.IntContext ctx) {
        String stringValue = ctx.getText();
        String hexValue = asHex(Integer.parseInt(stringValue));
        lineArray.add("\tMOVLW " + hexValue);
        return stringValue;
    }

    @Override
    public Object visitCondStmt(CompParser.CondStmtContext ctx) {
        int value = Integer.parseInt(ctx.expr().getText());
//        if (value == 1) {
//            lineArray.add("\tDECFSZ "+addr); // should be used when making actual comparisons
//        }
        lineArray.add("\tGOTO branch"+currentLabel);
        lineArray.add("\tGOTO out"+currentLabel);

        return super.visitCondStmt(ctx);
    }

    @Override
    public Object visitWhileExpr(CompParser.WhileExprContext ctx) {
        currentLabel = labelCounter;
        labelCounter++;
        lineArray.add("begin"+currentLabel);
        visit(ctx.cond()); // we visit the condition that is in the parenthesis
        lineArray.add("branch"+currentLabel); //beginning of while block
        visit(ctx.block()); // insert what's in the block
        lineArray.add("\tGOTO begin" + currentLabel);
        lineArray.add("out"+currentLabel); // getting out of the while loop
        currentLabel--;
        return null;
    }

    /** parenthesis */
    @Override
    public String visitParens(CompParser.ParensContext ctx) {
        return (String) visit(ctx.expr()); // return child expr's value
    }

    /** 
	 * Delay statement
	 * Creates 1s delay
	 */
    @Override
    public Object visitDelayStmt(CompParser.DelayStmtContext ctx) {
        lineArray.add("\tMOVLW 0xB");
        lineArray.add("\tMOVWF 0x72");
        lineArray.add("\tMOVLW 0x26");
        lineArray.add("\tMOVWF 0x71");
        lineArray.add("\tMOVLW 0x66");
        lineArray.add("\tMOVWF 0x70");
        lineArray.add("delay" + delayCounter);
        lineArray.add("\tDECFSZ 0x70, 1");
        lineArray.add("\tGOTO delay" + delayCounter);
        lineArray.add("\tDECFSZ 0x71, 1");
        lineArray.add("\tGOTO delay" + delayCounter);
        lineArray.add("\tDECFSZ 0x72, 1");
        lineArray.add("\tGOTO delay" + delayCounter);
        delayCounter++;
        return null;
    }

	/* Helping functions */

    /**
     * For printing out debug messages
     * @param message debug message
     */
    private void debugMsg(String message) {
        if (verbose) System.out.printf("[DEBUG] %s\n", message);
    }

    /**
     * Convert integer to hex format
     * @param address integer to be converted
     * @return String as hexadecimal. Format specified with constant.
     */
    private String asHex(int address) {

        return String.format(ADDRESS_FORMAT, address);
    }


    /**
     * Checks if given string is an integer and can be successfully converted to integer variable type.
     */
    private boolean isInt(String test) {
        return test.matches("^-?\\d+$");
    }

    /**
     * Finds the address where the variable has been assigned to.
     * @param var Name of the variable the address should be returned for.
     * @return Address as a hexadecimal number.
     */
    private String getVarAddress(String var) {
        Integer adr = variables.get(var);
        // if var not defined, throw exception
        if (adr == null)
            throw new LanguageException("Variable " + var + " not defined.");
        return asHex(adr);
    }
}
