package lang;

import java.io.*;
import org.antlr.v4.gui.Trees;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.cli.*;

/**
 * User: Hannes Kinks
 * Date: 12/13/13
 * Time: 10:22 PM
 */
public class Main {

    private static String srcFileName = null; // input source file (high level lang)
    private static String asmFileName = "data/out.asm"; // intermediate assembler output
    private static boolean verbose; // for debugging
    private static boolean drawTree; // draw parse tree

    public static void main(String[] args) {
        CommandLineParser commandLineParser = new DefaultParser();

        Options options = new Options();

        options.addOption("o", true, "Specify output file containing code compiled into assembler.");
        options.addOption("i", true, "Source file given as an input. If not specified, code will be entered from " +
                "standard input.");
        options.addOption("h", false, "Print usage information.");
        options.addOption("v", false, "Verbose.");
        options.addOption("t", false, "Show parse tree instead of running the code. " +
                "Also no output will be generated.");

        HelpFormatter formatter = new HelpFormatter();

        try {
            CommandLine cmd = commandLineParser.parse(options, args);
            InputStream is = System.in;

            verbose = cmd.hasOption("v");
            drawTree = cmd.hasOption("t");
            if (cmd.hasOption("h")) { //args.length == 0 || removed to make default behaviour to ask input from stdin
                formatter.printHelp("picoLang", options);
                return;
            }
            if (cmd.hasOption("o")) {
                asmFileName = cmd.getOptionValue("o");
            }
            if (cmd.hasOption("i")) {
                srcFileName = cmd.getOptionValue("i");
                is = new FileInputStream(srcFileName);
            } else {
                System.out.println("[INFO] Reading input from keyboard. Type the code and press CTRL+D to parse it.\n>");
            }

            // start compiling
            parseInputStream(is);

        } catch (ParseException e) {
            System.err.println("[ERR] Parsing of command line arguments failed.");
        } catch (FileNotFoundException e) {
            System.err.println("[ERR] File " + srcFileName + " was not found.");
        }
    }

    /**
     * Feeds input to ANTLR parser and starts the whole compiling process.
     * Input gets fed to lexer, parser, then abstract syntax tree is created, it will be
     * walked through and written into output file.
     * @param is input stream. In this case file or stdin.
     */
    private static void parseInputStream(InputStream is) {
        // feed input stream to antlr
        ANTLRInputStream input = null;
        try {
            input = new ANTLRInputStream(is);
        } catch (IOException e) {
            System.err.println("[ERR] Error occurred inputting file to ANTLR parser.");
        }
        CompLexer lexer = new CompLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CompParser parser = new CompParser(tokens);

        if (drawTree) {
            //draw tree in gui
            System.out.println("[INFO] Drawing tree.");
            ParserRuleContext ruleContext = parser.prog();
        /* updated in 4.5.1 */
            Trees.inspect(ruleContext, parser);
        /* deprecated: ruleContext.inspect(parser); */
        } else {
            //walk the tree
            ParseTree tree = parser.prog();
            EvalVisitor eval = new EvalVisitor(verbose);
            eval.visit(tree);

            // add ending statements
            // jumps to current program address counter and therefore stops running any code by being stuck in the cycle
            eval.lineArray.add("goto $");
            eval.lineArray.add("END");

            try {
                // write the lines to a asm file
                FileWriter fileWriter = new FileWriter(asmFileName);
                for (String line : eval.lineArray) {
                    if (!line.endsWith("\n")) // if newline symbol not in the end
                        fileWriter.write(line + "\n"); // append newline symbol to each line
                }
                fileWriter.close();
            } catch (IOException e) {
                System.err.println("[ERR] File cannot be created or opened. Make sure it is not already open somewhere " +
                        "or you have the required permissions.");
            }

        }
    }
}
